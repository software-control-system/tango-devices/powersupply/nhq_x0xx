
static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/NHQ_x0xx/src/NHQ_x0xxClass.cpp,v 1.4 2010-03-26 09:37:05 vince_soleil Exp $";

static const char *TagName = "$Name: not supported by cvs2svn $";

static const char *FileName= "$Source: /users/chaize/newsvn/cvsroot/Instrumentation/NHQ_x0xx/src/NHQ_x0xxClass.cpp,v $"; 

static const char *HttpServer= "http://controle/DeviceServer/doc/";

static const char *RCSfile = "$RCSfile: NHQ_x0xxClass.cpp,v $"; 

//+=============================================================================
//
// file :        NHQ_x0xxClass.cpp
//
// description : C++ source for the NHQ_x0xxClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the NHQ_x0xx once per process.
//
// project :     TANGO Device Server
//
// $Author: vince_soleil $
//
// $Revision: 1.4 $
//
// $Log: not supported by cvs2svn $
// Revision 1.3  2007/05/31 14:10:48  sebleport
// the 2 channels are now included in the device interface.
//
// Revision 1.2  2006/09/18 12:04:07  sebleport
// dev_status changes
//
// Revision 1.1.1.1  2006/03/08 08:51:38  stephle
// initial import
//
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <NHQ_x0xx.h>
#include <NHQ_x0xxClass.h>


namespace NHQ_x0xx_ns
{
//+----------------------------------------------------------------------------
//
// method : 		RestoreVoltageCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *RestoreVoltageCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "RestoreVoltageCmd::execute(): arrived" << endl;

	((static_cast<NHQ_x0xx *>(device))->restore_voltage());
	return new CORBA::Any();
}



//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
NHQ_x0xxClass *NHQ_x0xxClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::NHQ_x0xxClass(string &s)
// 
// description : 	constructor for the NHQ_x0xxClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
NHQ_x0xxClass::NHQ_x0xxClass(string &s):DeviceClass(s)
{

	cout2 << "Entering NHQ_x0xxClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving NHQ_x0xxClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::~NHQ_x0xxClass()
// 
// description : 	destructor for the NHQ_x0xxClass
//
//-----------------------------------------------------------------------------
NHQ_x0xxClass::~NHQ_x0xxClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
NHQ_x0xxClass *NHQ_x0xxClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new NHQ_x0xxClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

NHQ_x0xxClass *NHQ_x0xxClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void NHQ_x0xxClass::command_factory()
{
	command_list.push_back(new RestoreVoltageCmd("RestoreVoltage",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum NHQ_x0xxClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum NHQ_x0xxClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum NHQ_x0xxClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void NHQ_x0xxClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new NHQ_x0xx(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: NHQ_x0xxClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void NHQ_x0xxClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : voltageA
	voltageAAttrib	*voltage_a = new voltageAAttrib();
	Tango::UserDefaultAttrProp	voltage_a_prop;
	voltage_a_prop.set_label("VoltageA");
	voltage_a_prop.set_unit("V");
	voltage_a_prop.set_standard_unit("V");
	voltage_a_prop.set_display_unit("V");
	voltage_a_prop.set_description("represents the output voltage on channel A, this value can be modified");
	voltage_a->set_default_properties(voltage_a_prop);
	voltage_a->set_polling_period(2000);
	voltage_a->set_memorized();
	voltage_a->set_memorized_init(false);
	att_list.push_back(voltage_a);

	//	Attribute : maxVoltageA
	maxVoltageAAttrib	*max_voltage_a = new maxVoltageAAttrib();
	Tango::UserDefaultAttrProp	max_voltage_a_prop;
	max_voltage_a_prop.set_label("max Voltage A");
	max_voltage_a_prop.set_unit("V");
	max_voltage_a_prop.set_standard_unit("V");
	max_voltage_a_prop.set_display_unit("V");
	max_voltage_a_prop.set_description("this attribute is calculated according to a % of Vmax.\n- this % can be modify by turning the rotary switch \nreachable only in front of the Power Supply. \n- Vmaw is depending on the NHQ version. for the NHQ_203M\nversion Vmax = 3000V.\nFinally if % = 10% and Vmax = 3000V\ntherefore maxVoltage = 300 V.");
	max_voltage_a->set_default_properties(max_voltage_a_prop);
	max_voltage_a->set_polling_period(5000);
	att_list.push_back(max_voltage_a);

	//	Attribute : currentA
	currentAAttrib	*current_a = new currentAAttrib();
	Tango::UserDefaultAttrProp	current_a_prop;
	current_a_prop.set_label("Current A");
	current_a_prop.set_unit("µA");
	current_a_prop.set_standard_unit("µA");
	current_a_prop.set_display_unit("µA");
	current_a_prop.set_description("Represents the current generated on channel A, this value can be modified");
	current_a->set_default_properties(current_a_prop);
	current_a->set_polling_period(2000);
	current_a->set_memorized();
	current_a->set_memorized_init(false);
	att_list.push_back(current_a);

	//	Attribute : maxCurrentA
	maxCurrentAAttrib	*max_current_a = new maxCurrentAAttrib();
	Tango::UserDefaultAttrProp	max_current_a_prop;
	max_current_a_prop.set_label("max Current A");
	max_current_a_prop.set_unit("mA");
	max_current_a_prop.set_standard_unit("mA");
	max_current_a_prop.set_display_unit("mA");
	max_current_a_prop.set_description("this attribute is calculated according to a % of Imax.\n- this % can be modify by turning the rotary switch \nreachable only in front of the Power Supply. \n- Imax is depending on the NHQ version. for the NHQ_203M\nversion Vmax = 4 mA.\nFinally if % = 10% and Imax = 4 mA\ntherefore maxVoltage = 0.4 mA.");
	max_current_a->set_default_properties(max_current_a_prop);
	max_current_a->set_polling_period(5000);
	att_list.push_back(max_current_a);

	//	Attribute : rampSpeedA
	rampSpeedAAttrib	*ramp_speed_a = new rampSpeedAAttrib();
	Tango::UserDefaultAttrProp	ramp_speed_a_prop;
	ramp_speed_a_prop.set_label("Speed Ramp A ");
	ramp_speed_a_prop.set_unit("V/s");
	ramp_speed_a_prop.set_standard_unit("V/s");
	ramp_speed_a_prop.set_display_unit("V/s");
	ramp_speed_a_prop.set_max_value("255");
	ramp_speed_a_prop.set_min_value("2");
	ramp_speed_a_prop.set_description("Represents the velocity for reaching the voltage preset on channel A");
	ramp_speed_a->set_default_properties(ramp_speed_a_prop);
	ramp_speed_a->set_polling_period(3000);
	ramp_speed_a->set_memorized();
	ramp_speed_a->set_memorized_init(false);
	att_list.push_back(ramp_speed_a);

	//	Attribute : voltageB
	voltageBAttrib	*voltage_b = new voltageBAttrib();
	Tango::UserDefaultAttrProp	voltage_b_prop;
	voltage_b_prop.set_label("Voltage B");
	voltage_b_prop.set_unit("V");
	voltage_b_prop.set_standard_unit("V");
	voltage_b_prop.set_display_unit("V");
	voltage_b_prop.set_description("represents the output voltage on channel B, this value can be modified");
	voltage_b->set_default_properties(voltage_b_prop);
	voltage_b->set_polling_period(2000);
	voltage_b->set_memorized();
	voltage_b->set_memorized_init(false);
	att_list.push_back(voltage_b);

	//	Attribute : maxVoltageB
	maxVoltageBAttrib	*max_voltage_b = new maxVoltageBAttrib();
	Tango::UserDefaultAttrProp	max_voltage_b_prop;
	max_voltage_b_prop.set_label("max Voltage B");
	max_voltage_b_prop.set_unit("V");
	max_voltage_b_prop.set_standard_unit("V");
	max_voltage_b_prop.set_display_unit("V");
	max_voltage_b_prop.set_description("this attribute is calculated according to a % of Vmax.\n- this % can be modify by turning the rotary switch \nreachable only in front of the Power Supply. \n- Vmaw is depending on the NHQ version. for the NHQ_203M\nversion Vmax = 3000V.\nFinally if % = 10% and Vmax = 3000V\ntherefore maxVoltage = 300 V.");
	max_voltage_b->set_default_properties(max_voltage_b_prop);
	max_voltage_b->set_polling_period(5000);
	att_list.push_back(max_voltage_b);

	//	Attribute : currentB
	currentBAttrib	*current_b = new currentBAttrib();
	Tango::UserDefaultAttrProp	current_b_prop;
	current_b_prop.set_label("Current B");
	current_b_prop.set_unit("µA");
	current_b_prop.set_standard_unit("µA");
	current_b_prop.set_display_unit("µA");
	current_b_prop.set_description("Represents the current generated on channel B, this value can be modified");
	current_b->set_default_properties(current_b_prop);
	current_b->set_polling_period(2000);
	current_b->set_memorized();
	current_b->set_memorized_init(false);
	att_list.push_back(current_b);

	//	Attribute : maxCurrentB
	maxCurrentBAttrib	*max_current_b = new maxCurrentBAttrib();
	Tango::UserDefaultAttrProp	max_current_b_prop;
	max_current_b_prop.set_label("max Current B");
	max_current_b_prop.set_unit("mA");
	max_current_b_prop.set_standard_unit("mA");
	max_current_b_prop.set_display_unit("mA");
	max_current_b_prop.set_description("this attribute is calculated according to a % of Imax.\n- this % can be modify by turning the rotary switch \nreachable only in front of the Power Supply. \n- Imax is depending on the NHQ version. for the NHQ_203M\nversion Vmax = 4 mA.\nFinally if % = 10% and Imax = 4 mA\ntherefore maxVoltage = 0.4 mA.");
	max_current_b->set_default_properties(max_current_b_prop);
	max_current_b->set_polling_period(5000);
	att_list.push_back(max_current_b);

	//	Attribute : rampSpeedB
	rampSpeedBAttrib	*ramp_speed_b = new rampSpeedBAttrib();
	Tango::UserDefaultAttrProp	ramp_speed_b_prop;
	ramp_speed_b_prop.set_label("Speed Ramp B");
	ramp_speed_b_prop.set_unit("V/s");
	ramp_speed_b_prop.set_standard_unit("V/s");
	ramp_speed_b_prop.set_display_unit("V/s");
	ramp_speed_b_prop.set_max_value("255");
	ramp_speed_b_prop.set_min_value("2");
	ramp_speed_b_prop.set_description("Represents the velocity for reaching the voltage preset on channel B");
	ramp_speed_b->set_default_properties(ramp_speed_b_prop);
	ramp_speed_b->set_polling_period(3000);
	ramp_speed_b->set_memorized();
	ramp_speed_b->set_memorized_init(false);
	att_list.push_back(ramp_speed_b);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void NHQ_x0xxClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	NHQ_x0xxClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void NHQ_x0xxClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "SerialProxyName";
	prop_desc = "name of the Serial device";
	prop_def  = "";
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "AutoStart";
	prop_desc = "true: autostart actived (the output voltages are automatically applied)\nfalse: autostart desactived\n\n";
	prop_def  = "true";
	vect_data.clear();
	vect_data.push_back("true");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		NHQ_x0xxClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void NHQ_x0xxClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("NHQ_x0xx");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("This DeviceServers controls NHQ_x0xx series of high voltage powersupplies provided by FAST or ISEG supplier");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs location
	string	rcsId(RcsId);
	string	filename(classname);
	start = rcsId.find("/");
	if (start!=string::npos)
	{
		filename += "Class.cpp";
		end   = rcsId.find(filename);
		if (end>start)
		{
			string	strloc = rcsId.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}

	//	Get CVS tag revision
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
